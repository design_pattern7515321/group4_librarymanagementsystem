package com.libraryapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.libraryapp.entities.User;
import com.libraryapp.services.UserService;

@Service
public class CurrentUserFinder {

	@Autowired
	UserService usService; // It is injecting the UserService bean to interact with user-related data.

	// Static instance for Singleton
	private static CurrentUserFinder instance;

	// Private constructor to prevent creating instance outside the class
	private CurrentUserFinder() {
	}

	// Public static method to get the instance
	public static CurrentUserFinder getInstance() {
		if (instance == null) {
			synchronized (CurrentUserFinder.class) {
				if (instance == null) {
					instance = new CurrentUserFinder();
				}
			}
		}
		return instance;
	}

	public long getCurrentUserId() {
		UserDetails details = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = details.getUsername();
		long userId = -1;
		for (User user : usService.findAll()) {
			if (user.getUserName().equals(username)) {
				userId = user.getUserId();
				break;
			}
		}
		return userId;
	}

	public User getCurrentUser() {
		User currentUser = usService.findById(getCurrentUserId());
		return currentUser;
	}
}
