package com.libraryapp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.libraryapp.entities.User;
import com.libraryapp.security.CurrentUserFinder;
import com.libraryapp.services.UserService;

import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

	@Autowired
	private UserService usService;

	@Autowired
	private CurrentUserFinder currentUserFinder;

	// Template Method
	private String performOperation(String operation, Model model, boolean accStatus, String role, Long userId) {
		User user = usService.findById(userId);
		model.addAttribute("user", user);
		model.addAttribute("role", role);
		model.addAttribute("accStatus", accStatus);
		return "admin/admin-" + operation + ".html";
	}

	@GetMapping
	public String adminHome(Model model) {
		User currentUser = currentUserFinder.getCurrentUser();
		model.addAttribute("currentUser", currentUser);
		return "admin/admin-home.html";
	}

	@GetMapping(value = "/manageaccounts")
	public String manageAccounts(Model model, @RequestParam(required = false) String firstName,
			@RequestParam(required = false) String lastName) {
		List<User> users = usService.userSearcher(firstName, lastName);
		model.addAttribute("users", users);
		model.addAttribute("firstName", firstName);
		model.addAttribute("lastName", lastName);
		return "admin/admin-manage-accounts.html";
	}

	@GetMapping(value = "/manageaccount")
	public String manageAccount(Model model, @RequestParam Long userId) {
		return performOperation("manage-account", model, false, null, userId);
	}

	@PutMapping(value = "/confirmaccountsettings")
	public String confirmAccountChanges(Model model, @RequestParam boolean accStatus, @RequestParam String role,
			@RequestParam Long userId) {
		return performOperation("confirm-account-settings", model, accStatus, role, userId);
	}

	@PutMapping(value = "/saveaccountsettings")
	public String saveAccountSettings(@RequestParam boolean accStatus, @RequestParam String role,
			@RequestParam Long userId) {
		User user = usService.findById(userId);
		user.setRole(role);
		user.setEnabled(accStatus);
		usService.save(user);
		return "redirect:/admin/accountsettingssaved";
	}

	@GetMapping(value = "/accountsettingssaved")
	public String accountSettingsSaved() {
		return "admin/admin-account-settings-saved.html";
	}
}
