package com.libraryapp.controllers;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
public class HomeController {

	interface RedirectHandler {
		String handleRequest();

		void nextHandler(RedirectHandler nextHandler);
	}

	static class AdminRedirectHandler implements RedirectHandler {
		private RedirectHandler nextHandler;

		@Override
		public void nextHandler(RedirectHandler nextHandler) {
			this.nextHandler = nextHandler;
		}

		@Override
		public String handleRequest() {
			UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Collection<? extends GrantedAuthority> roles = principal.getAuthorities();
			List<String> roleNames = new ArrayList<>();
			for (GrantedAuthority role : roles) {
				roleNames.add(role.getAuthority());
			}
			if (roleNames.contains("ROLE_ADMIN")) {
				return "redirect:/admin";
			} else if (nextHandler != null) {
				return nextHandler.handleRequest();
			} else {
				return "redirect:/"; // Default redirect if no role matches
			}
		}
	}

	static class EmployeeRedirectHandler implements RedirectHandler {
		private RedirectHandler nextHandler;

		@Override
		public void nextHandler(RedirectHandler nextHandler) {
			this.nextHandler = nextHandler;
		}

		@Override
		public String handleRequest() {
			UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Collection<? extends GrantedAuthority> roles = principal.getAuthorities();
			List<String> roleNames = new ArrayList<>();
			for (GrantedAuthority role : roles) {
				roleNames.add(role.getAuthority());
			}
			if (roleNames.contains("ROLE_EMPLOYEE")) {
				return "redirect:/employee";
			} else if (nextHandler != null) {
				return nextHandler.handleRequest();
			} else {
				return "redirect:/"; // Default redirect if no role matches
			}
		}
	}

	static class UserRedirectHandler implements RedirectHandler {
		private RedirectHandler nextHandler;

		@Override
		public void nextHandler(RedirectHandler nextHandler) {
			this.nextHandler = nextHandler;
		}

		@Override
		public String handleRequest() {
			UserDetails principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Collection<? extends GrantedAuthority> roles = principal.getAuthorities();
			List<String> roleNames = new ArrayList<>();
			for (GrantedAuthority role : roles) {
				roleNames.add(role.getAuthority());
			}
			if (roleNames.contains("ROLE_USER")) {
				return "redirect:/user";
			} else if (nextHandler != null) {
				return nextHandler.handleRequest();
			} else {
				return "redirect:/"; // Default redirect if no role matches
			}
		}
	}

	private RedirectHandler redirectChain;

	public HomeController() {
		RedirectHandler adminHandler = new AdminRedirectHandler();
		RedirectHandler employeeHandler = new EmployeeRedirectHandler();
		RedirectHandler userHandler = new UserRedirectHandler();

		adminHandler.nextHandler(employeeHandler);
		employeeHandler.nextHandler(userHandler);

		redirectChain = adminHandler;
	}

	@GetMapping(value = "/")
	public String redirectToHome() {
		return redirectChain.handleRequest();
	}
}
