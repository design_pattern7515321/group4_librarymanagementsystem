package com.libraryapp.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.RequestDispatcher;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import java.util.HashMap;
import java.util.Map;

@Controller
public class ErrorPagesController implements ErrorController {

	private static final String GENERAL_ERROR_PAGE = "error/general-error-page.html";

	// Define strategy methods using lambda expressions
	private final Map<Integer, HttpStatusHandler> strategyMap = new HashMap<Integer, HttpStatusHandler>() {
		{
			put(HttpStatus.BAD_REQUEST.value(), () -> "error-pages/400-error.html");
			put(HttpStatus.FORBIDDEN.value(), () -> "error-pages/403-error.html");
			put(HttpStatus.NOT_FOUND.value(), () -> "error-pages/404-error.html");
			put(HttpStatus.REQUEST_TIMEOUT.value(), () -> "error-pages/408-error.html");
			put(HttpStatus.INTERNAL_SERVER_ERROR.value(), () -> "error-pages/500-error.html");
			put(HttpStatus.BAD_GATEWAY.value(), () -> "error-pages/502-error.html");
			put(HttpStatus.SERVICE_UNAVAILABLE.value(), () -> "error-pages/503-error.html");
			put(HttpStatus.GATEWAY_TIMEOUT.value(), () -> "error-pages/504-error.html");
		}
	};

	@RequestMapping(value = "/error")
	public String errorHandler(HttpServletRequest request) {
		Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
		if (status != null) {
			int statusCode = Integer.valueOf(status.toString());
			return strategyMap.getOrDefault(statusCode, () -> GENERAL_ERROR_PAGE).handle();
		}
		return GENERAL_ERROR_PAGE;
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}

	// Define functional interface for strategy
	@FunctionalInterface
	private interface HttpStatusHandler {
		String handle();
	}
}
