package com.libraryapp.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.libraryapp.DAO.NotificationRepository;
import com.libraryapp.entities.Notification;

@Service
public class NotificationService {

	@Autowired
	private NotificationRepository notifRepo;

	// Mediator Interface
	private interface Mediator {
		void sendNotification(Notification notification);

		void deleteNotification(Long id);
	}

	// Concrete Mediator
	private class NotificationServiceMediator implements Mediator {
		@Override
		public void sendNotification(Notification notification) {
			save(notification); // Delegate to NotificationService method
		}

		@Override
		public void deleteNotification(Long id) {
			deleteById(id); // Delegate to NotificationService method
		}
	}

	private final Mediator mediator = new NotificationServiceMediator();

	// Existing methods remain unchanged

	public void save(Notification notification) {
		notifRepo.save(notification);
	}

	public void saveById(Long id) {
		Notification notification = notifRepo.findById(id).orElse(null);
		if (notification != null) {
			save(notification);
		}
	}

	public List<Notification> findAll() {
		return (ArrayList<Notification>) notifRepo.findAll();
	}

	public void deleteById(Long id) {
		notifRepo.deleteById(id);
	}
}
