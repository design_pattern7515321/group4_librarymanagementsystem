package com.libraryapp;

import java.time.LocalDate;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.libraryapp.entities.Book;
import com.libraryapp.entities.User;
import com.libraryapp.services.BookService;
import com.libraryapp.services.NotificationService;
import com.libraryapp.services.UserService;
import com.libraryapp.utils.MidnightApplicationRefresh;

@SpringBootApplication
public class LibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}

	@Autowired
	BookService bookService;

	@Autowired
	UserService usService;

	@Autowired
	NotificationService notifService;

	@Autowired
	BCryptPasswordEncoder pwEncoder;

	@Autowired
	MidnightApplicationRefresh midAppRef;

	@Bean
	CommandLineRunner runner() {
		return args -> {

			User user1 = new User("admin", pwEncoder.encode("test"), "drukden@gmail.com", "Druk", "Dorji",
					"Huizumerlaan 158", "06-11433823", "Amsterdam");
			user1.setRole("ROLE_ADMIN");

			User user2 = new User("employee", pwEncoder.encode("test"), "Jampel.dorji@gamail.com", "Jample", "Dorji",
					"Hugo de Grootstraat 174", "06-87054875", "Sliedrecht");
			user2.setRole("ROLE_EMPLOYEE");

			User user3 = new User("drukden", pwEncoder.encode("test"), "drukden.wangchuk@gmail.com", "Drukden",
					"Wangchuk",
					"Leidijk 97", "06-18756892", "Groningen");
			user3.setRole("ROLE_USER");
			User user4 = new User("aniemies", pwEncoder.encode("test"), "annemie.schuurbiers@gmail.com", "Annemie",
					"Schuurbiers", "Duinerlaan 173", "06-83472443", "Eelde");
			User user5 = new User("chencho", pwEncoder.encode("test"), "chencho.dema@gmail.com", "Chencho", "Dema",
					"Paro 90", "06-13644621", "Paro");
			User user6 = new User("pema", pwEncoder.encode("test"), "pema.yangzom@gmail.com", "Pema", "Yangzom",
					"Paro 60", "06-13644621", "Tashigang");

			usService.save(user1);
			usService.save(user2);
			usService.save(user3);
			usService.save(user4);
			usService.save(user5);
			usService.save(user6);

			Book book1 = new Book("Prj", "David Thomas, Andrew Hunt", 2006, 1);
			Book book2 = new Book("Cyber Security", "Robert C. Martin", 2020, 2);
			Book book3 = new Book("Design Pattern", "Steve McConnell", 2012, 1);
			Book book4 = new Book("Maths", "Martin Fowler", 2017, 4);
			Book book5 = new Book("Secure Coding",
					"Eric Freeman, Bert Bates, Kathy Sierra, Elisabeth Robson", 2019, 5);
			Book book6 = new Book("Enterprise Blockchain", "Frederick P. Brooks Jr", 1999, 1);

			bookService.save(book1);
			bookService.save(book2);
			bookService.save(book3);
			bookService.save(book4);
			bookService.save(book5);
			bookService.save(book6);

			book2.setTheUser(user3);
			book2.setReturnDate(LocalDate.of(2021, 5, 23));

			book1.setTheUser(user3);
			book1.setReturnDate(LocalDate.of(2021, 5, 05));

			user3.setBooks(Arrays.asList(book2, book1));

			bookService.save(book1);
			bookService.save(book2);
			usService.save(user3);

			midAppRef.midnightApplicationRefresher();
		};
	}
}
